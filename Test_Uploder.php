<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>title</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- jquery 3.2.1 -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!-- jQuery UI 1.12.1 -->
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<!-- bootstrap　4.2.1 -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	
	<script>
	// スマホで決定ボタンをおした時にformがsubmitされるのを防ぐ。キーボードを閉じる挙動で代替させる。
	$(function(){
		// スマホで決定ボタンをおした時にformがsubmitされるのを防ぐ。キーボードを閉じる挙動で代替させる。
		$('input').on('keypress', (e) => {
			if(e.keyCode === 13){
				e.preventDefault();

				// 現在フォーカスしている入力欄からフォーカスを外すことで、キーボードを強制的に閉じる
				document.activeElement.blur();
				$('input').blur();
			}
		});
	});
	</script>
</head>
<body>
<form id="SampForm">	
<div class="form-group">
	<label for="exampleInputFile">File input</label>
	<input type="file" name="file" id="file">

	<p class="help-block">ファイルを選択して下さい。</p>

	<div id="Up_progress_Result_9999"></div><!--upload状況-->
	
	

	<div class="progress progress-striped active">
		<div id="Up_progress_9999" class="progress-bar progress-bar-success" style="width: 0%"></div>
	</div>
	<div class="UploadResult"></div><!--アップロードの結果が入る-->
</div>

<a class="btn btn-lg btn-success btn-block SampFormSubmitBtn" id="SampFormSubmitBtn" data-contentid="9999">保存</a>
	
</form>
	
	
<script>
$(function(){
	$('.SampFormSubmitBtn').on('click',function(){
		$('#Up_progress_9999').css('width', '0%');
		$('#Up_progress_Result_9999').text('アップロード中');
		var form = $('#SampForm').get()[0];
		var formData = new FormData( form );
		var ContentIds = $('#SampFormSubmitBtn').attr('data-contentid');
			$.ajax({
			 xhr : function() {
                XHR = $.ajaxSettings.xhr();
                if (XHR.upload) {
                    XHR.upload.addEventListener('progress',
                            function(e) {
                                progre = parseInt(e.loaded / e.total * 10000) / 100;
                                console.log(progre + "%");
								$('#Up_progress_9999').css('width', progre + '%');
//                                document.getElementById(p_id).value = progre;
//                                document.getElementById(p_id).innerHTML = progre + '%';
								//アップロードファイルの確認
								if(progre == 100){
									$(function(){
										setTimeout(function(){
											$('#Up_progress_Result_9999').text('アップロード完了');
											$('#Up_progress_9999').css('width',  '0%');
										},1000);
									});									
									
									
								   }
                            }, false);
                }
                return XHR;
            },				
			type: "POST",
			type: "POST",
			url: "./Test_Upload_Ajax.php" ,
			processData: false ,// Ajaxがdataを整形しない指定
			contentType: false ,// contentTypeもfalseに指定
			data: formData,
		
			dataType: 'json',
				//処理が完了すると実行される部分。
				 success: function(data){
					 //alert("更新しました");  //文字列の表示
					 $(".UploadResult").html('<span>アップロード時Name:'+data.or_file+'/アップロードName:'+data.up_file_name+'/DIR:'+data.dir+'</span>'); 
				 },
				// 処理に失敗すると実行される部分
				 error:function(){
					 alert("upload失敗しました");  //文字列の表示
					 $(".UploadResult").html('upload失敗しました');
				 }
			});

		return false;
	});
});
</script>
</body>
</html>
